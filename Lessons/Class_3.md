**Introducción:**

Al inicio de cada clase se deben realizar de 10 a 15 minutos de ejercicios para motivar al grupo, al final se termina con una actividad grupal para ir generando lazos entre el grupo.

Recordatorio de la clase anterior de 10 a 20 minutos.

**RECOMENDACIONES Y USOS DE LOS MATERIALES ELECTRÓNICOS.**

No se deben colocar los materiales en la boca, ojos, nariz, tampoco punzar o molestar usando materiales a los demás compañeros, estos son peligrosos y pueden provocar heridas.

El profesor debe tomar el primer componente a explicar y luego de la explicación dar al alumno para que lo revise y pueda verificar lo explicado.

**LED:**

Un LED tiene tres partes principales:

- **La parte de plástico:** Esta es la parte que ves y que puede ser de diferentes colores, como rojo, verde, azul, amarillo, entre otros. Esta parte protege las partes internas del LED y también ayuda a difundir la luz.
    
- **Los cables o patitas:** Son como las piernas del LED. Se conectan a un circuito para darle electricidad y hacer que brille.
    
- **El chip dentro del LED:** Es la parte mágica que hace que el LED brille cuando le llega electricidad. Cuando la electricidad pasa por el chip, emite luz.
   

**Usos de un LED:**

Los LEDs se usan en muchas cosas que ves a tu alrededor:

- **Luces de Navidad:** Seguro que has visto luces de Navidad brillantes. Muchas de estas luces son en realidad pequeños LEDs.
    
- **Linternas y lucecitas**: Algunas linternas y lucecitas que se usan para ver en la oscuridad también tienen LEDs en su interior.
    
- **Pantallas de dispositivos electrónicos:** Muchos dispositivos como tu televisor, la pantalla de tu reloj digital o la luz de tu tableta utilizan LEDs para mostrarte imágenes o información.
    

Es importante indicar que el LED no se debe conectar directamente a una batería ya que estos funciona a partir de 1,5 hasta 3 voltios y mas de ese voltage los queman.

**RESISTENCIAS:**

- **Funcionamiento de una Resistencia:**

	Imagina que una resistencia es como una especie de policía para la electricidad. Cuando la electricidad está corriendo muy rápido y fuerte, la resistencia la reduce y la hace más tranquila. Esto es importante para que los aparatos eléctricos no se rompan.

- **Partes de una Resistencia:**
    

	Una resistencia es como un pequeño cilindro o tubito. Tiene dos patitas o cables en cada extremo. La electricidad entra por una patita, pasa a través de la resistencia y sale por la otra patita. La resistencia está hecha de un material especial que dificulta el paso de la electricidad, haciendo que sea más lenta y segura.

- **Usos de una Resistencia:**
    

Las resistencias se usan en muchas cosas diferentes:

- **Luces:** En algunas luces, las resistencias ayudan a controlar la cantidad de luz que sale para que no sea demasiado brillante.
    
- **Electrodomésticos:** Algunos electrodomésticos como las tostadoras o los secadores de pelo tienen resistencias que ayudan a controlar la temperatura para que no se calienten demasiado.
    
- **Juguetes:** Incluso en algunos juguetes, las resistencias ayudan a que funcionen de manera segura y correcta.
    

**Conductores:**

Los conductores son materiales que dejan pasar fácilmente la electricidad. Esto significa que permiten que los electrones (partículas que llevan la electricidad) se muevan libremente a través de ellos. Algunos ejemplos de conductores son:

- **Metales:** Como el cobre, el aluminio y el hierro. Por eso se usan cables de cobre para llevar la electricidad a nuestras casas.
    
- **Agua salada:** El agua con sal disuelta en ella también puede conducir electricidad.

**No Conductores (Aislantes):**

Los no conductores, o aislantes, son materiales que no permiten que la electricidad pase a través de ellos fácilmente. Los electrones no pueden moverse libremente a través de estos materiales. Algunos ejemplos de no conductores son:

- **Plástico:** Como el que se usa para hacer carcasas de teléfonos o juguetes.
    
- **Madera:** La madera no conduce electricidad, por eso es seguro tocarla incluso si hay cables eléctricos cerca.
    
- **Vidrio:** El vidrio tampoco conduce electricidad, por eso es seguro usar vasos de vidrio.

**Semiconductores:**

Los semiconductores están en algún punto entre los conductores y los no conductores. Pueden conducir electricidad mejor que los no conductores, pero no tan bien como los conductores. Los semiconductores son muy importantes en la electrónica moderna y se usan en cosas como:

- **Transistores:** Son componentes electrónicos que controlan el flujo de electricidad en los circuitos. Los transistores están hechos de materiales semiconductores.
    
- **Diodos LED:** Los diodos LED son luces pequeñas y eficientes que también están hechas de semiconductores.
    
- **Circuitos integrados**: Los chips de computadora y otros dispositivos electrónicos contienen muchos semiconductores que hacen que funcionen.