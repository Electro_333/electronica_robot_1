**Introducción.-**  

Al inicio de cada clase se deben realizar de 10 a 15 minutos de ejercicios para motivar al grupo, al final se termina con una actividad grupal para ir generando lazos entre el grupo.

Presentación del profesor:

Hola soy **[Nombre y Apellido]** (colocar en la pizarra).

Pueden decirme Profe, **[Nombre]**, se debe considerar ser un amigo para el grupo.

Iniciamos una breve descripción de las normas del aula y uso debido del baño u otras instancias del aula.

En caso de que alguien se sienta mal o tenga algún problema, se debe informar al profesor de forma inmediata y este informará al area adecuada para comunicarse con el padre de familia.

Se continúa con la importancia de la robótica y electrónica.

**¿Qué es la electrónica?**

Imagina que la electrónica es como un rompecabezas muy interesante. Cada pieza (como los cables, los chips o los componentes) tiene un trabajo específico. Al unir estas piezas de la manera correcta, puedes hacer que las cosas funcionen de la forma que quieras.

_**Ejemplos:**_

Funcionamiento de carros a control remoto, funcionamiento del celular y computadoras.

**¿Qué son los robots?**

Los robots son como amigos mecánicos que puedes construir y programar para que hagan cosas increíbles. Pueden moverse, hablar, e incluso hacer tareas útiles como limpiar o entregar cosas.

_**Ejemplos:**_

Robots de entrega en Amazon, robots en la construcción de automóviles.

__Primera actividad:__

Dibujar un robot con forma de carro, colocar un nombre y hacer una descripción del uso y funcionamiento del mismo.

Cada alumno debe pasar al frente, o ponerse de pie para decir en voz alta su nombre, la escuela donde estudia, el nombre del robot y su uso y funcionamiento.

Siempre el profesor debe ir repitiendo en voz alta lo expresado por el alumno para que todos logren escuchar.

Finalizar la  clase con instrucciones o materiales necesarios para la siguiente clase.